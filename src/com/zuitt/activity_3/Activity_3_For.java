package com.zuitt.activity_3;
import java.util.Scanner;

public class Activity_3_For {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.println("Input an integer whose factorial will be computed");

    int num = 0;
    try{
      num = in.nextInt();
    } catch (Exception e) {
      System.out.println("Invalid Input");
      e.printStackTrace();
    }

    int answer = 1;
    for (int counter = 1; counter <= num; counter++) {
      answer = counter * answer;
    }

    if (num > 0) {
      System.out.println("The factorial of " + num + " is " + answer);
    } else {
      System.out.println("Please enter a valid positive integer");
    }
  }
}
