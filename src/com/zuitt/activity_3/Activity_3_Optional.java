package com.zuitt.activity_3;

public class Activity_3_Optional {
  public static void main(String[] args) {
    for (int i = 1; i <= 5; i++) {
      String line = "";
      for (int j = 1; j <= i; j++) {
        line += "* ";
      }
      System.out.println(line);
    }
  }
}
